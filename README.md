# ALP4 Tutorial-12

This branch contains all materials for the 12 tutorial session.

## Agenda

- Assignment's solution presentation
- Recap & Discussion: exam preparation, more on Web Development
- Q&A
