---
title: Finale
layout: center
---

# Last Tutorial Session

---
title: Reminders
---

## Reminders

<br />

- Last tutorial session
- Last chance to present (vorrechnen) your assignment sheet

## Today's topics

<br />

- Exam preparation
- More on Web Development

---
title: Exam Preparation
layout: two-cols
---

## Relevant topics

- Concurrency, Determinism
- Critical Sections, Threads, Locks
- Petri Nets
- Semaphores, Monitors
- Deadlocks
- Parallel Programming (OpenMP, MPI)
- IPC/RPC
- P2P Networks
- Cloud Computing
- Web Development

::right::

## Non-relevant topics

- Java RMI (Coding, but not theory)

<!-- <div class="container flex justify-center mt-5"> -->
<!--     <img src="/images/RPC.webp" class="block w-md"/> -->
<!-- </div> -->

---
title: Concurrency and Parallelism
---

## Some important questions I

<br />

- Deterministic vs Determined
- What is a critical section? How to protect it?
- What are the requirements for a solution that protects a critical section?
- Why do we need hardware support for mutual exclusion?
- Differences between threads and processes
- What are the conditions for a deadlock?
- How can we handle deadlocks?
- What are the differences between semaphores and monitors?
- Why do we use condition variables in monitors?
- What are the differences between OpenMP and MPI?
- What are the steps of Foster's methodology?
- What is Amdahl's Law?

**Make sure to familiarize yourself with the terminology and the concepts, as well as the code examples from the lectures.**

---
title: Distributed Systems
---

## Some important questions II

<br />

- What is IPC/RPC? What are the differences?
- What is P2P network? What are the differences between P2P and Client-Server?
- Categories of P2P networks and their characteristics.
- What is cloud computing? What are the differences between IaaS, PaaS, and SaaS?
- What are the differences between public, private, and hybrid clouds?
- What are HTML, JavaScript, and CSS used for? (Be able to read code and understand what it does)
- What is HTTP/S?
- Undertand the structure of a web page, and how it interacts with the server.
- Basic knowledge of ways to develop a frontend and a backend application.

**Make sure to familiarize yourself with the terminology and the concepts, as well as the code examples from the lectures.**

---
title: Banker's Algorithm
---

## Banker's Algorithm I

Calculate $v$ and $B$ and give an termination order of the threads if the situation is safe.

<br />

$$
G = \begin{pmatrix}
    5 & 3 & 3 & 1 \\
    4 & 3 & 1 & 3 \\
    5 & 6 & 4 & 3 \\
    2 & 2 & 0 & 0 \\
    6 & 5 & 3 & 2
\end{pmatrix}

R = \begin{pmatrix}
    4 & 3 & 2 & 1 \\
    1 & 2 & 0 & 1 \\
    4 & 5 & 3 & 2 \\
    0 & 1 & 0 & 0 \\
    5 & 3 & 1 & 1
\end{pmatrix}

f = \begin{pmatrix} 0 & 1 & 0 & 1 \end{pmatrix}
$$

---
title: Banker's Algorithm
---

## Banker's Algorithm II

Calculate $f$ and $R$ and give an termination order of the threads if the following situation is safe.

<br />

$$
G = \begin{pmatrix}
    5 & 11 & 10 \\
    8 & 6 & 11 \\
    7 & 3 & 5 \\
\end{pmatrix}

B = \begin{pmatrix}
    3 & 10 & 9 \\
    6 & 4 & 7 \\
    2 & 0 & 4 \\
\end{pmatrix}

v = \begin{pmatrix} 13 & 15 & 22 \end{pmatrix}
$$
